//
//  ViewController.swift
//  DownloadImages
//
//  Created by Click Labs on 3/24/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //image view use to show downloaded images...
    @IBOutlet weak var downloadedImages: UIImageView!
    //cache variable use to store downloaded images...
    var imageCache = [String : UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //button code to download images with given url...
    @IBAction func downloadImages(sender: AnyObject) {
        //specified url from where image downloaded...
        var fileURL: NSString = "http://upload.wikimedia.org/wikipedia/commons/thumb/2/27/Leonardo_da_Vinci_-_Adorazione_dei_Magi_-_Google_Art_Project.jpg/250px-Leonardo_da_Vinci_-_Adorazione_dei_Magi_-_Google_Art_Project.jpg"
        
        //call function, responsible for downloading images..
        getImageFromURL(fileURL)
    }
    //function which is responsible for downloading images..
    func getImageFromURL(fileURL : NSString)
    {
        var url: NSURL!
        url = NSURL(string: fileURL)
        var image = self.imageCache[fileURL]
        var err: NSError?
        //code block which check into cache memory whether image is already downloaded or not...if not then
        if( image == nil ){
            var imgURL: NSURL = NSURL(string: fileURL)!
            let request: NSURLRequest = NSURLRequest(URL: imgURL)
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: {(response: NSURLResponse!,data: NSData!,error: NSError!) -> Void in
                if error == nil {
                    image = UIImage(data: data)
                    
                    // Store the image in to our cache
                    self.imageCache[fileURL] = image
                }
                else {
                    println("Error: \(error.localizedDescription)")
                }
            })
            
            //these line of code download the image and save images into document directory...
            var imageData :NSData = NSData(contentsOfURL: url, options: NSDataReadingOptions.DataReadingMappedIfSafe, error: &err)!
            var bgImage = UIImage(data:imageData)
            let pathsArray = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
            let documentsDirectory = pathsArray[0] as String
            println(documentsDirectory)
            let savePath = documentsDirectory.stringByAppendingPathComponent("image-FileName.jpg")
            UIImagePNGRepresentation(bgImage).writeToFile(savePath, atomically: true)
            
        }//otherwise shows alert...
        else{
            let title = "Already Downloaded!!"
            let message = " Image is present in cache memory..."
            let okText = "Ok"
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
            let okButton = UIAlertAction(title: okText, style: UIAlertActionStyle.Cancel, handler: nil)
            alert.addAction(okButton)
            presentViewController(alert, animated: true, completion: nil)
        }
        
        //assign cache image to image view...
        dispatch_async(dispatch_get_main_queue(), {
            self.downloadedImages.image = image
        })
    }
}

